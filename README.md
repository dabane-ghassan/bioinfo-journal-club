# Bioinformatics Journal Club

In anticipation of my upcoming internship at the Timone Neuroscience Institue [(INT)](http://www.int.univ-amu.fr/), i'm going to prepare a small presentation for my master's journal club about the work that i'm going to do during my training period.

## TODO

1. Continue reading the paper/papers of course ..... (Done)
2. Try to iron out important details
3. Working on the first draft
4. Continue working on the TODO list in free time

## Nice ideas to implement

- Use reveal.js
- Use manim
  
## References

[[1] *Daucé, E., Albiges, P., & Perrinet, L. U. (2020).A dual foveal-peripheral visual processing model implements efficient saccade selection. Journal of Vision 20(8):22*](https://doi.org/10.1167/jov.20.8.22)

[[2] *Jaderberg, M., Simonyan, K., & Zisserman, A. (2015).Spatial transformer networks. In Advances in neural information processing systems (pp. 2017-2025)*](https://arxiv.org/pdf/1506.02025.pdf)


[[3] *Ha, David (2016).Generating Large Images from Latent Vectors. blog.otoro.net*](https://blog.otoro.net/2016/04/01/generating-large-images-from-latent-vectors/)
